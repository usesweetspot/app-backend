<?php

    // SweetSpot Configuration File (v1.0)
    // Developers: Jesse Stauffer, Casey Stauffer, Frank Patel
    
    // Increase Memory Limit (Photo Uploads)
    ini_set("memory_limit", "1024M");
    
    // Connect to Database
    //mysql_connect(HOSTNAME,USERNAME, PASSWORD) OR DIE ("Unable to connect to database! Please try again later.");
    //mysql_select_db("sweetspot");
    
    // Handle Errors
    function display_error($err_code)
    {
        switch ($err_code)
        {
            case 1:
                echo 'An invalid controller is being referenced.';
                break;
            case 2:
                echo 'An invalid method is being referenced.';
                break;
            case 3:
                echo 'The connection was unsuccessful.';
                break;
            case 100:
                echo 'Please enter a username.';
                break;
            case 101:
                echo 'Please enter an email address.';
                break;
            case 102:
                echo 'Please enter a password.';
                break;
            case 103:
                echo 'This username has already been registered.';
                break;
            case 104:
                echo 'This email address has already been registered.';
                break;
            case 105:
                echo 'Please enter your username or password.';
                break;
            case 106:
                echo 'Please enter your password.';
                break;
            case 107:
                echo 'Your login credentials are invalid. Please try again.';
                break;
            case 200:
                echo 'Please provide a user ID for the sending user.';
                break;
            case 201:
                echo 'Please provide a username to add.';
                break;
            case 202:
                echo 'The friend request was not found.';
                break;
            case 203:
                echo 'Please provide a user ID for the requesting user.';
                break;
            case 204:
                echo 'The user was not found. Please try again.';
                break;
            default:
                echo 'An invalid error code was provided.';
        }
        exit(0);
    }
    
    // Show Success Message
    function display_success()
    {
        echo 'SUCCESS';
    }
    
?>