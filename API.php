<?php

	// SweetSpot API (v1.0)
        // Developers: Jesse Stauffer, Casey Stauffer, Frank Patel
        // To be used ONLY by authenticated clients (The SweetSpot mobile app)

        include('config.php');
        
        $parameters = $_REQUEST;
        $controller = ucfirst(strtolower($parameters['controller']));
        $method = strtolower($parameters['method']);
        
        // Check Controller Validity
        $controller_path = "controllers/{$controller}.php";
        if (file_exists($controller_path))
            include_once $controller_path;
        else
            display_error(1);
            
        // Check Method Validity
        $controller = new $controller($parameters);
        if (!method_exists($controller, $method))
            display_error(2);
            
        // Make Method Call
        $controller->$method();
?>