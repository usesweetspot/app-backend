<?php

    // SweetSpot Users Endpoint (v1.0)
    // Developers: Jesse Stauffer, Casey Stauffer, Frank Patel
    ini_set('display_errors',1);
error_reporting(E_ALL);

    class Users
    {
        private $_parameters;
        
        public function __construct($parameters)
        {
            $this->_parameters = $parameters;
        }
        
        // Create Account Method
        // Creates a new user account on SweetSpot
        public function create_account()
        {
            // Retrieve Local Parameters
            $user_name = mysql_escape_string($_POST['user_name']);
            $email_address = mysql_escape_string($_POST['email_address']);
            $password = sha1(mysql_escape_string($_POST['password']));
            
            if ($user_name == NULL)
                display_error(100);
            else if ($email_address == NULL)
                display_error(101);
            else if ($password == NULL)
                display_error(102);
            
            // Check Username Validity
            $getUsersByUsername = mysql_query("SELECT user_name FROM users WHERE user_name='".$user_name."'");
            if (mysql_num_rows($getUsersByUsername) == 0)
            {
                // Check Email Validity
                $getUsersByEmailAddress = mysql_query("SELECT email_address FROM users WHERE email_address='".$email_address."'");
                if (mysql_num_rows($getUsersByEmailAddress) == 0)
                {
                    // Create User Account
                    $signUpUser = mysql_query("INSERT INTO users (user_name, email_address, password) VALUES ('".$user_name."', '".$email_address."', '".$password."')");
                    $data = array("user_id" => mysql_insert_id());
                    display_success($data);
                }
                else
                    display_error(104);
            }
            else
                display_error(103);
        }
        
        // Log In Method
        // Confirms a user's authenticated credentials
        public function log_in()
        {
            // Retrieve Local Parameters
            $user_field = mysql_escape_string($_POST['user_field']);
            $password = sha1(mysql_escape_string($_POST['password']));
            
            if ($user_field == NULL)
                display_error(105);
            else if ($password == NULL)
                display_error(106);
                
            // Check User Credentials
            $checkUserCredentials = mysql_query("SELECT user_id FROM users WHERE (user_name='".$user_field."' OR email_address='".$user_field."') AND password='".$password."' LIMIT 1");
            if (mysql_num_rows($checkUserCredentials) > 0)
            {
                $results = mysql_fetch_array($checkUserCredentials);
                $data = array("user_id" => $results['user_id']);
                display_success($data);
            }
            else
                display_error(107);
        }
        
        // Update Profile Photo Method
        // Modifies a user's current profile photo
        public function update_profile_photo()
        {
            // Retrieve Local Parameters
            $user_id = mysql_escape_string($_POST['user_id']);
            if (is_uploaded_file($_FILES['file']['tmp_name']))
            {
                // Retrieve Photo
                $uploadDir = '/var/www/html/sweetspot/images/users/';
                $file = basename($_FILES['file']['name']);
                $filePath = $uploadDir.$user_id.$file;
                
                $image = imagecreatefromjpeg($_FILES['file']['tmp_name']);
                $max_width = 120;
                $max_height = 120;
                $old_width = imagesx($image);
                $old_height = imagesy($image);
                
                $scale = min($max_width/$old_width, $max_height/$old_height);
                $new_width = ceil($scale*$old_width);
                $new_height = ceil($scale*$old_height);
                
                $new = imagecreatetruecolor($new_width, $new_height);
                imagecopyresampled($new, $image, 0, 0, 0, 0, $new_width, $new_height, $old_width, $old_height);
                imagejpeg($new, $_FILES['file']['tmp_name'], 90);
                move_uploaded_file($_FILES['file']['tmp_name'], $filePath) OR die("wahhh");
            }
        }
    }
    
?>