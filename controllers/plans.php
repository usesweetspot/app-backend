<?php

    // SweetSpot Plans Endpoint (v1.0)
    // Developers: Jesse Stauffer, Casey Stauffer, Frank Patel
    
    class Plans
    {
        private $_parameters;
        
        public function __construct($parameters)
        {
            $this->_parameters = $parameters;
        }
        
        // Get Plans Method
        // Retrieves an authenticated users' plans
        public function get_plans()
        {
            // Retrieve Local Parameters
            $user_id = mysql_escape_string($_POST['user_id']);
            
            $getPlans = mysql_query("SELECT plan_id FROM userplans WHERE user_id='".$user_id."'");
            $tableData = array();
            while ($row = mysql_fetch_assoc($getPlans))
            {
                $getPlanInfo = mysql_query("SELECT creator_id, name, winner_selection_id FROM plans WHERE plan_id='".$row['plan_id']."'");
                $planInfo = mysql_fetch_array($getPlanInfo);
                $planData = array("creator_id" => $planInfo['creator_id'], "plan_name" => $planInfo['name'], "winning_selection" => $planInfo['winner_selection_id']);
                $checkDidVote = mysql_query("SELECT * FROM votes WHERE user_id='".$user_id."' AND plan_id='".$row['plan_id']."'");
                if (mysql_num_rows($checkDidVote) > 0)
                    $didVoteData = array("did_vote" => "YES");
                else
                    $didVoteData = array("did_vote" => "NO");
                $tableData[] = $row + $planData + $didVoteData;
            }
            $data = array("data" => $tableData);
            display_success($data);
        }
        
        // Create Plans Method
        // Creates a new plan
        public function create_plan()
        {
            // Retrieve Local Parameters
            $creator_id = mysql_escape_string($_POST['creator_id']);
            $name = mysql_escape_string($_POST['name']);
            $createPlan = mysql_query("INSERT INTO plans (creator_id, name) VALUES ('".$creator_id."', '".$name."')");
            $data = array("plan_id" => mysql_insert_id());
            display_success($data);
        }
        
        // Create Selection Method
        // Creates a new selection
        public function create_selection()
        {
            // Retrieve Local Parameters
            $plan_id = mysql_escape_string($_POST['plan_id']);
            $name = mysql_escape_string($_POST['name']);
            $yelp_id = mysql_escape_string($_POST['yelp_id']);
            $latitude = mysql_escape_string($_POST['latitude']);
            $longitude = mysql_escape_string($_POST['longitude']);
            $createSelection = mysql_query("INSERT INTO selections (plan_id, name, yelp_id, latitude, longitude) VALUES ('".$plan_id."', '".$name."', '".$yelp_id."', '".$latitude."', '".$longitude."')");
            display_success();
        }
        
        // Remove Plan Method
        // Removes an authenticated user from a plan
        public function remove_plan()
        {
            // Retrieve Local Parameters
            $user_id = mysql_escape_string($_POST['user_id']);
            $plan_id = mysql_escape_string($_POST['plan_id']);
            $removePlan = mysql_query("DELETE FROM userplans WHERE user_id='".$user_id."' AND plan_id='".$plan_id."'");
            $getAllUsers = mysql_query("SELECT * FROM userplans WHERE plan_id='".$plan_id."'");
            if (mysql_num_rows($getAllUsers) == 0)
            {
                $deletePlan = mysql_query("DELETE FROM plans WHERE plan_id='".$plan_id."'");
                $deleteSelections = mysql_query("DELETE FROM selections WHERE plan_id='".$plan_id."'");
            }
            display_success();
        }
        
        // Get Plan Members
        // Returns the members of a plan
        public function get_plan_members()
        {
            // Retrieve Local Parameters
            $plan_id = mysql_escape_string($_POST['plan_id']);
            $getMembers = mysql_query("SELECT user_id FROM userplans WHERE plan_id='".$plan_id."'");
            $tableData = array();
            while ($row = mysql_fetch_assoc($getMembers))
            {
                $getUserInfo = mysql_query("SELECT user_name FROM users WHERE user_id='".$row['user_id']."'");
                $userInfo = mysql_fetch_array($getUserInfo);
                $userData = array("user_name" => $userInfo['user_name']);
                $tableData[] = $row + $userData;
            }
            $data = array("data" => $tableData);
            display_success($data);
        }
        
        // Add User Plan
        // Adds a user to a plan
        public function add_userplan()
        {
            $plan_id = mysql_escape_string($_POST['plan_id']);
            $user_id = mysql_escape_string($_POST['user_id']);
            $removePlans = mysql_query("DELETE FROM userplans WHERE user_id='".$user_id."' AND plan_id='".$plan_id."'");
            $addPlan = mysql_query("INSERT INTO userplans (user_id, plan_id) VALUES ('".$user_id."', '".$plan_id."')");
            display_success();
        }
        
        // Get Selections
        // Returns the selections for the plan
        public function get_selections()
        {
            // Retrieve Local Parameters
            $plan_id = mysql_escape_string($_POST['plan_id']);
            $latitude = floatval(mysql_escape_string($_POST['latitude']));
            $longitude = floatval(mysql_escape_string($_POST['longitude']));
            $getSelections = mysql_query("SELECT selection_id, name, latitude, longitude, yelp_id FROM selections WHERE plan_id='".$plan_id."'");
            $tableData = array();
            while ($row = mysql_fetch_assoc($getSelections))
            {
                $distanceData = array("distance" => haversineGreatCircleDistance($latitude, $longitude, $row['latitude'], $row['longitude']));
                $tableData[] = $row + $distanceData;
            }
            $data = array("data" => $tableData);
            display_success($data);
        }
        
        // Get Selection Info
        // Returns information about the selection
        public function get_selection_info()
        {
            // Retrieve Local Parameters
            $selection_id = mysql_escape_string($_POST['selection_id']);
            $latitude = floatval(mysql_escape_string($_POST['latitude']));
            $longitude = floatval(mysql_escape_string($_POST['longitude']));
            $getSelections = mysql_query("SELECT selection_id, name, latitude, longitude, yelp_id FROM selections WHERE selection_id='".$selection_id."'");
            $tableData = array();
            $row = mysql_fetch_assoc($getSelections);
            $distanceData = array("distance" => haversineGreatCircleDistance($latitude, $longitude, $row['latitude'], $row['longitude']));
            $tableData[] = $row + $distanceData;
            $data = array("data" => $tableData);
            display_success($data);
        }
        
        // Cast Vote
        // Casts a user's vote for a selection
        public function cast_vote()
        {
            // Retrieve Local Parameters
            $user_id = mysql_escape_string($_POST['user_id']);
            $plan_id = mysql_escape_string($_POST['plan_id']);
            $selection_id = mysql_escape_string($_POST['selection_id']);
            
            $addVote = mysql_query("INSERT INTO votes (plan_id, user_id, selection_id) VALUES ('".$plan_id."', '".$user_id."', '".$selection_id."')");
            $getMembers = mysql_query("SELECT * FROM userplans WHERE plan_id='".$plan_id."'");
            $getVotes = mysql_query("SELECT * FROM votes WHERE plan_id='".$plan_id."'");
            
            if (mysql_num_rows($getMembers) == mysql_num_rows($getVotes))
            {
                // election over
                $selection1 = 0;
                $selection2 = 0;
                $selection3 = 0;
                $selection4 = 0;
                $selection5 = 0;
                
                $counter = 0;
                
                $selection1value = 0;
                $selection2value = 0;
                $selection3value = 0;
                $selection4value = 0;
                $selection5value = 0;
                
                $getSelections = mysql_query("SELECT * FROM selections WHERE plan_id='".$plan_id."'");
                while ($selectionResults = mysql_fetch_array($getSelections))
                {
                    $counter++;
                    switch ($counter)
                    {
                        case 1:
                            $selection1value = $selectionResults['selection_id'];
                            break;
                        case 2:
                            $selection2value = $selectionResults['selection_id'];
                            break;
                        case 3:
                            $selection3value = $selectionResults['selection_id'];
                            break;
                        case 4:
                            $selection4value = $selectionResults['selection_id'];
                            break;
                        case 5:
                            $selection5value = $selectionResults['selection_id'];
                            break;
                    }
                }
                
                while ($voteResults = mysql_fetch_array($getVotes))
                {
                    if ($voteResults['selection_id'] == $selection1value)
                        $selection1++;
                    if ($voteResults['selection_id'] == $selection2value)
                        $selection2++;
                    if ($voteResults['selection_id'] == $selection3value)
                        $selection3++;
                    if ($voteResults['selection_id'] == $selection4value)
                        $selection4++;
                    if ($voteResults['selection_id'] == $selection5value)
                        $selection5++;
                }
                
                $winner = max($selection1, $selection2, $selection3, $selection4, $selection5);
                
                switch ($winner)
                {
                    case $selection1:
                        $winnerValue = $selection1value;
                        break;
                    case $selection2:
                        $winnerValue = $selection2value;
                        break;
                    case $selection3:
                        $winnerValue = $selection3value;
                        break;
                    case $selection4:
                        $winnerValue = $selection4value;
                        break;
                    case $selection5:
                        $winnerValue = $selection5value;
                        break;
                }
                
                $addResult = mysql_query("UPDATE plans SET winner_selection_id='".$winnerValue."' WHERE plan_id='".$plan_id."'");
            }
            
            display_success();
        }
    }
    
?>