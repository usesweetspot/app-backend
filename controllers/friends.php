<?php

    // SweetSpot Friends Endpoint (v1.0)
    // Developers: Jesse Stauffer, Casey Stauffer, Frank Patel
    
    class Friends
    {
        private $_parameters;
        
        public function __construct($parameters)
        {
            $this->_parameters = $parameters;
        }
        
        // Add Friend Method
        // Allows a user to send a friend request
        public function add_friend()
        {
            // Retrieve Local Parameters
            $user_id = mysql_escape_string($_POST['user_id']);
            $username = mysql_escape_string($_POST['username']);
            
            if ($user_id == NULL)
                display_error(200);
            else if ($username == NULL)
                display_error(201);
                
            $getUserID = mysql_query("SELECT user_id FROM users WHERE user_name='".$username."' LIMIT 1");
            
            if (mysql_num_rows($getUserID) == 0)
                display_error(204);
            else
            {
                $userInfo = mysql_fetch_array($getUserID);
                $user_id2 = $userInfo['user_id'];   
            }
            
            $deleteRequests = mysql_query("DELETE FROM friends WHERE friend_id1='".$user_id1."' AND friend_id2='".$user_id2."'");
            
            // Check User Credentials
            if (intval($user_id) != intval($user_id2))
            {
                $addFriend = mysql_query("INSERT INTO friends (friend_id1, friend_id2, status) VALUES ('".$user_id."', '".$user_id2."', 'pending')");
            }
            display_success();
        }
        
        // Accept Friend Request
        // Allows a user to accept a friend request
        public function accept_friend_request()
        {
            // Retrieve Local Parameters
            $user_id1 = mysql_escape_string($_POST['friend_id1']);
            $user_id2 = mysql_escape_string($_POST['friend_id2']);
            
            if ($user_id1 == NULL)
                display_error(200);
            else if ($user_id2 == NULL)
                display_error(201);
                
            // Accept Friend Request
            $acceptRequest = mysql_query("UPDATE friends SET status='accepted' WHERE (friend_id1='".$user_id1."' AND friend_id2='".$user_id2."') OR (friend_id1='".$user_id2."' AND friend_id2='".$user_id1."')");
            display_success();
        }
        
        // Delete Friend
        // Allows a user to delete a friend
        public function delete_friend()
        {
            // Retrieve Local Parameters
            $user_id1 = mysql_escape_string($_POST['friend_id1']);
            $user_id2 = mysql_escape_string($_POST['friend_id2']);
            
            if ($user_id1 == NULL)
                display_error(200);
            else if ($user_id2 == NULL)
                display_error(201);
                
            // Delete Friend
            $deleteFriend = mysql_query("DELETE FROM friends WHERE (friend_id1='".$user_id1."' AND friend_id2='".$user_id2."') OR (friend_id1='".$user_id2."' AND friend_id2='".$user_id1."')");
            if ($deleteFriend)
                display_success();
            else
                display_error(3);
        }
        
        // Show Friends
        // Retrieves the friends of an authenticated user
        public function get_friends()
        {
            // Retrieve Local Parameters
            $user_id = mysql_escape_string($_POST['user_id']);
            
            if ($user_id == NULL)
                display_error(203);
                
            // Get Friends
            $getFriends = mysql_query("SELECT * FROM friends WHERE (friend_id1='".$user_id."' OR friend_id2='".$user_id."') AND status='accepted'");
            $return_array = array();
            while ($result = mysql_fetch_assoc($getFriends))
            {
                if ($result['friend_id1'] == intval($user_id))
                    $theirID = $result['friend_id2'];
                else
                    $theirID = $result['friend_id1'];
                    
                $getUserInformation = mysql_query("SELECT user_name FROM users WHERE user_id='".$theirID."'");
                $userResults = mysql_fetch_array($getUserInformation);
                $user_info = array("user_id" => $theirID, "user_name" => $userResults['user_name']);
                $return_array[] = $user_info;
            }
            $data = array("data" => $return_array);
            display_success($data);
        }
        
        // Get Friend Requests
        // Returns pending friend requests for an authenticated user
        public function get_friend_requests()
        {
            // Retrieve Local Parameters
            $user_id = mysql_escape_string($_POST['user_id']);
            
            if ($user_id == NULL)
                display_error(203);
                
            // Get Friend Requests
            $getFriendRequests = mysql_query("SELECT * FROM friends WHERE friend_id2='".$user_id."' AND status='pending'");
            $return_array = array();
            while ($result = mysql_fetch_assoc($getFriendRequests))
            {
                if ($result['friend_id1'] == intval($user_id))
                    $theirID = $result['friend_id2'];
                else
                    $theirID = $result['friend_id1'];
                    
                $getSenderInformation = mysql_query("SELECT user_name FROM users WHERE user_id='".$theirID."'");
                $senderResults = mysql_fetch_array($getSenderInformation);
                $sender_info = array("user_id" => $theirID, "user_name" => $senderResults['user_name']);
                $return_array[] = $result + $sender_info;
            }
            $data = array("data" => $return_array);
            display_success($data);
        }
    }
    
?>