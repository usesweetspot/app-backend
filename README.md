# README #

SweetSpot is a location-based group decision making application that helps groups of friends plan the perfect night out.

### App Backend ###

This repository houses all of SweetSpot's backend API source files. The mobile client and website connect to this API to access and mutate application data.

### Configuration ###
The config.php file is used to connect to the database. When uploading to the server, we will need to insert the HOST_NAME, USER_NAME, and PASSWORD fields. 